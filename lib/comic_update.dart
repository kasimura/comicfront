import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';
import 'images_page.dart';
import 'index.dart';
import 'database.dart';
import 'bookshelf.dart';
import 'package:dio/dio.dart';

class ComicUpdatePage extends StatelessWidget {
  const ComicUpdatePage({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Comics Recent Updated")),
      body: FutureBuilder<List<Index>>(
        future: _getComics(),
        builder: (BuildContext context, AsyncSnapshot<List<Index>> snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                return new ListTile(
                  leading: FadeInImage.memoryNetwork(
                      placeholder: kTransparentImage,
                      image: snapshot.data[index].thumb,
                      fit: BoxFit.contain),
                  title: Text(snapshot.data[index].name),
                  onTap: () async {
                    Book book = await DBProvider.db
                        .getBook(int.parse(snapshot.data[index].id));
                    Index idx;
                    if (book != null) {
                      idx = new Index(
                          id: book.id.toString(),
                          name: book.name,
                          thumb: book.thumb,
                          version: book.last);
                    } else {
                      idx = snapshot.data[index];
                    }
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ImagesPage(),
                        settings: RouteSettings(arguments: idx),
                      ),
                    );
                  },
                );
              },
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }

  Future<List<Index>> _getComics() async {
    Dio dio = new Dio();
    final response = await dio.get('https://comicsserver.herokuapp.com/update');
    List<Index> tempList = [];
    for (int i = 0; i < response.data.length; i++) {
      tempList.add(Index.fromJson(response.data[i]));
    }
    return tempList;
  }
}
