import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'comic_history.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:dio/dio.dart';
import 'index.dart';
import 'images_page.dart';
import 'comic_update.dart';
import 'database.dart';
import 'bookshelf.dart';

const appBarHeight = 50.0;

class ComicPage extends StatefulWidget {
  @override
  _ComicPageState createState() => new _ComicPageState();
}

class _ComicPageState extends State<ComicPage> {
  final TextEditingController _filter = new TextEditingController();
  final dio = new Dio();
  String _searchText = "";
  List<Index> comics = [];
  List<Index> filteredComics = [];
  double hasComics = 0.0;
  Icon _searchIcon = new Icon(Icons.search);
  Widget _appBarTitle = new Text('Search Comic');

  _ComicPageState() {
    _filter.addListener(() {
      if (_filter.text.isEmpty) {
        setState(() {
          _searchText = "";
        });
      } else {
        setState(() {
          _searchText = _filter.text;
        });
      }
      filteredComics = comics;
    });
  }

  @override
  void initState() {
    this._getComics();
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildBar(context),
      //drawer: ComicDrawer(),
      body: Container(
        child: _buildList(),
      ),
      resizeToAvoidBottomPadding: false,
      bottomNavigationBar: _buildBottomBar(context),
    );
  }

  //void _onAdd() {}

  Widget _buildBottomBar(BuildContext context) {
    return BottomAppBar(
      //shape: CircularNotchedRectangle(),
      child: Row(
        children: <Widget>[
          IconButton(
            icon: Icon(Icons.menu_book),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ComicUpdatePage()),
              );
            },
          ),
          //SizedBox(),
          IconButton(
            icon: Icon(Icons.book_sharp),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ComicHistoryPage()),
              );
            },
          )
        ],
        mainAxisAlignment: MainAxisAlignment.spaceAround,
      ),
    );
  }

  Widget _buildBar(BuildContext context) {
    return new PreferredSize(
      preferredSize: Size.fromHeight(appBarHeight),
      child: AppBar(
        centerTitle: true,
        title: _appBarTitle,
        leading: new IconButton(
          icon: _searchIcon,
          onPressed: _searchPressed,
        ),
        bottom: _createProgressIndicator(),
      ),
    );
  }

  Widget _buildList() {
    if (_searchText.isNotEmpty) {
      List<Index> tempList = [];
      for (int i = 0; i < filteredComics.length; i++) {
        if (filteredComics[i].name.contains(_searchText)) {
          tempList.add(filteredComics[i]);
        }
      }
      filteredComics = tempList;
    }
    return ListView.builder(
      itemCount: comics == null ? 0 : filteredComics.length,
      itemBuilder: (BuildContext context, int index) {
        return new ListTile(
          leading: FadeInImage.memoryNetwork(
              placeholder: kTransparentImage,
              image: filteredComics[index].thumb,
              fit: BoxFit.contain),
          title: Text(filteredComics[index].name),
          onTap: () async {
            Book book = await DBProvider.db
                .getBook(int.parse(filteredComics[index].id));
            Index idx;
            if (book != null) {
              idx = new Index(
                  id: book.id.toString(),
                  name: book.name,
                  thumb: book.thumb,
                  version: book.last);
            } else {
              idx = filteredComics[index];
            }
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => ImagesPage(),
                settings: RouteSettings(arguments: idx),
              ),
            );
          },
        );
      },
    );
  }

  void _searchPressed() {
    setState(() {
      if (this._searchIcon.icon == Icons.search) {
        this._searchIcon = new Icon(Icons.close);
        this._appBarTitle = new TextField(
          controller: _filter,
          autofocus: true,
          decoration: new InputDecoration(
              prefixIcon: new Icon(Icons.search), hintText: 'Search...'),
        );
      } else {
        this._searchIcon = new Icon(Icons.search);
        this._appBarTitle = new Text('Search Comic');
        filteredComics = comics;
        _filter.clear();
      }
    });
  }

  void _getComics() async {
    final response = await dio.get('https://comicsserver.herokuapp.com/');
    List<Index> tempList = [];
    for (int i = 0; i < response.data.length; i++) {
      tempList.add(Index.fromJson(response.data[i]));
    }
    setState(() {
      hasComics = 1.0;
      comics = tempList;
      filteredComics = comics;
    });
  }

  PreferredSize _createProgressIndicator() => PreferredSize(
      preferredSize: Size(double.infinity, 4.0),
      child: SizedBox(
          height: 4.0, child: LinearProgressIndicator(value: hasComics)));
}
