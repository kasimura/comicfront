import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:dio/dio.dart';
import 'index.dart';
import 'comic.dart';
import 'bookshelf.dart';
import 'database.dart';

const appBarHeight = 50.0;

Future<Comic> fetchComicPages(String id) async {
  final dio = new Dio();
  final response =
      await dio.get('https://comicsserver.herokuapp.com/comic/' + id);
  return Comic.fromJson(response.data);
}

class ImagesPage extends StatefulWidget {
  @override
  _ImagesPageState createState() => new _ImagesPageState();
}

class _ImagesPageState extends State<ImagesPage> {
  bool _showAppbar = false;
  bool upperScreen = false;
  Text _actionBarTitle = Text("");
  int pageIndex = 0;
  PageController pageController;

  @override
  void dispose() {
    pageController.dispose();
    super.dispose();
  }

  void _checkPointScreenPosition(BuildContext context, Offset position) {
    RenderBox box = context.findRenderObject();
    upperScreen = (position.dy < (box.size.height / 2));
  }

  Widget _buildBar(BuildContext context) {
    return _showAppbar
        ? new PreferredSize(
            preferredSize: Size.fromHeight(appBarHeight),
            child: AppBar(
              centerTitle: true,
              title: _actionBarTitle,
              leading: new IconButton(
                icon: const Icon(Icons.arrow_back),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ),
          )
        : PreferredSize(
            child: Container(),
            preferredSize: Size(0.0, 0.0),
          );
  }

  Widget _buildActionButton(BuildContext context) {
    return _showAppbar
        ? Stack(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: 31),
                child: Align(
                  alignment: Alignment.bottomLeft,
                  child: FloatingActionButton(
                    child: const Icon(Icons.arrow_left),
                    onPressed: () {
                      pageController.previousPage(
                          curve: Curves.linear,
                          duration: Duration(milliseconds: 100));
                    },
                    heroTag: null,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomRight,
                child: FloatingActionButton(
                  child: const Icon(Icons.arrow_right),
                  onPressed: () {
                    pageController.nextPage(
                        curve: Curves.linear,
                        duration: Duration(milliseconds: 100));
                  },
                  heroTag: null,
                ),
              ),
            ],
          )
        : null;
  }

  Widget _buildGallery(BuildContext context, Comic comic, Index idx) {
    pageIndex = idx.version != "" ? comic.images.indexOf(idx.version) : 0;
    pageController = PageController(initialPage: pageIndex);
    _actionBarTitle = Text("${pageIndex + 1} / ${comic.images.length}");
    return Listener(
      onPointerDown: (pointer) {
        _checkPointScreenPosition(context, pointer.position);
      },
      child: GestureDetector(
        onTap: () => {
          if (upperScreen)
            {
              pageController.previousPage(
                  curve: Curves.linear, duration: Duration(milliseconds: 100))
            }
          else
            {
              pageController.nextPage(
                  curve: Curves.linear, duration: Duration(milliseconds: 100))
            }
        },
        child: PageView(
          pageSnapping: false,
          physics: NeverScrollableScrollPhysics(),
          controller: pageController,
          scrollDirection: Axis.vertical,
          onPageChanged: (index) async {
            pageIndex = index;
            Book book = Book(
              id: int.parse(comic.id),
              name: idx.name,
              thumb: idx.thumb,
              last: comic.images[index],
            );
            Book bookFromDB = await DBProvider.db.getBook(int.parse(comic.id));
            if (bookFromDB == null) {
              await DBProvider.db.newBook(book);
            } else {
              await DBProvider.db.updateBook(book);
            }
            setState(() {
              _actionBarTitle = Text("${index + 1} / ${comic.images.length}");
            });
          },
          children: comic.images
              .map((image) =>
                  OrientationBuilder(builder: (context, orientation) {
                    return Center(
                        child: orientation == Orientation.portrait
                            ? _portraitView(image)
                            : _landscapeView(image));
                  }))
              .toList(),
        ),
      ),
    );
  }

  Widget _portraitView(String image) {
    return InteractiveViewer(
      panEnabled: true, // Set it to false to prevent panning.
      scaleEnabled: true,
      constrained: true,
      boundaryMargin: EdgeInsets.all(0),
      minScale: 0.5,
      maxScale: 2.0,
      child: Image.network(
        image,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        fit: BoxFit.scaleDown,
        alignment: Alignment.center,
      ),
    );
  }

  Widget _landscapeView(String image) {
    return InteractiveViewer(
      panEnabled: true, // Set it to false to prevent panning.
      scaleEnabled: true,
      constrained: false,
      boundaryMargin: EdgeInsets.all(0),
      minScale: 0.5,
      maxScale: 2.0,
      child: Image.network(
        image,
        width: MediaQuery.of(context).size.width,
        fit: BoxFit.fitWidth,
        alignment: Alignment.topLeft,
      ),
    );
  }

  Widget build(BuildContext context) {
    final Index index = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: _buildBar(context),
      body: FutureBuilder<Comic>(
        future: fetchComicPages(index.id),
        builder: (context, snapshot) {
          if (snapshot.hasError) print(snapshot.error);

          return snapshot.hasData
              ? GestureDetector(
                  onDoubleTap: () {
                    setState(() {
                      _showAppbar = !_showAppbar;
                    });
                  },
                  child: _buildGallery(context, snapshot.data, index),
                )
              : Center(
                  child: Container(
                    width: 20.0,
                    height: 20.0,
                    child: CircularProgressIndicator(),
                  ),
                );
        },
      ),
      floatingActionButton: _buildActionButton(context),
    );
  }
}
