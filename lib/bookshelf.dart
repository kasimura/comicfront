import 'dart:convert';

Book bookFromJson(String str) {
  final jsonData = json.decode(str);
  return Book.fromMap(jsonData);
}

String bookToJson(Book data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class Book {
  final int id;
  final String name;
  final String thumb;
  final String last;

  Book({this.id, this.name, this.thumb, this.last});

  factory Book.fromMap(Map<String, dynamic> json) => new Book(
        id: json["id"],
        name: json["name"],
        thumb: json["thumb"],
        last: json["last"],
      );
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'thumb': thumb,
      'last': last,
    };
  }
}
