class Index {
  final String id;
  final String name;
  final String thumb;
  final String version;
  Index({this.id, this.name, this.thumb, this.version});

  factory Index.fromJson(Map<String, dynamic> json) {
    return Index(
      id: json['id'],
      name: json['name'],
      thumb: json['thumb'],
      version: "",
    );
  }
}
