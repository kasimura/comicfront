class Comic {
  final String id;
  final List<String> images;
  Comic({this.id, this.images});

  factory Comic.fromJson(Map<String, dynamic> json) {
    var imagesFromJSON = json['images'];
    List<String> imagesList = List<String>.from(imagesFromJSON);
    return Comic(
      id: json['id'],
      images: imagesList,
    );
  }
}
