import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';
import 'bookshelf.dart';
import 'database.dart';
import 'images_page.dart';
import 'index.dart';

class ComicHistoryPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Comics History")),
      body: FutureBuilder<List<Book>>(
        future: DBProvider.db.getAllBooks(),
        builder: (BuildContext context, AsyncSnapshot<List<Book>> snapshot) {
          if (snapshot.hasData && snapshot.data != null) {
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                Book item = snapshot.data[index];
                return Dismissible(
                  key: UniqueKey(),
                  background: Container(color: Colors.red),
                  onDismissed: (direction) {
                    DBProvider.db.deleteBook(item.id);
                  },
                  child: ListTile(
                    title: Text(item.name),
                    leading: FadeInImage.memoryNetwork(
                        placeholder: kTransparentImage,
                        image: item.thumb,
                        fit: BoxFit.contain),
                    onTap: () async {
                      Index index = new Index(
                          id: item.id.toString(),
                          name: item.name,
                          thumb: item.thumb,
                          version: item.last);
                      Book book = await DBProvider.db.getBook(item.id);
                      Index idx;
                      if (book != null) {
                        idx = new Index(
                            id: book.id.toString(),
                            name: book.name,
                            thumb: book.thumb,
                            version: book.last);
                      } else {
                        idx = index;
                      }

                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ImagesPage(),
                          settings: RouteSettings(arguments: idx),
                        ),
                      );
                    },
                  ),
                );
              },
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }
}
