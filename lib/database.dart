import 'dart:async';
import 'dart:io' show Directory;
import 'package:flutter/foundation.dart' show kIsWeb;

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'bookshelf.dart';
import 'package:sqflite/sqflite.dart';

const tableName = 'bookshelf';

class DBProvider {
  DBProvider._();

  static final DBProvider db = DBProvider._();

 Database _database;

  Future<Database> get database async {
    if (kIsWeb) return null;

    if (_database != null) return _database;
    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  initDB() async {
    if (kIsWeb) return;
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "comics.db");
    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute(
        "CREATE TABLE " +
            tableName +
            "(id INTEGER PRIMARY KEY, name TEXT, thumb TEXT, last TEXT)",
      );
    });
  }

  newBook(Book book) async {
    if (kIsWeb) return null;

    final db = await database;
    var raw = await db.rawInsert(
        "INSERT Into " +
            tableName +
            "(id,name,thumb,last)"
                " VALUES (?,?,?,?)",
        [book.id, book.name, book.thumb, book.last]);
    return raw;
  }

  updateBook(Book book) async {
    if (kIsWeb) return null;

    final db = await database;
    var res = await db
        .update(tableName, book.toMap(), where: "id = ?", whereArgs: [book.id]);
    return res;
  }

  getBook(int id) async {
    if (kIsWeb) return null;

    final db = await database;
    var res = await db.query(tableName, where: "id = ?", whereArgs: [id]);
    return res.isNotEmpty ? Book.fromMap(res.first) : null;
  }

  Future<List<Book>> getAllBooks() async {
    if (kIsWeb) return null;

    final db = await database;
    var res = await db.query(tableName);
    List<Book> list =
        res.isNotEmpty ? res.map((c) => Book.fromMap(c)).toList() : [];
    return list;
  }

  deleteBook(int id) async {
    if (kIsWeb) return null;

    final db = await database;
    return db.delete(tableName, where: "id = ?", whereArgs: [id]);
  }

  deleteAll() async {
    final db = await database;
    db.rawDelete("Delete * from " + tableName);
    return null;
  }
}
