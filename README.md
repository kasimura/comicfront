# hello_world

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## Web

### Fast chrome with CORS disabled
On Windows
1. Make a temp folder, eg, E:\TEMP
2. Update the argument --user-data-dir and run below command
`"C:\Program Files\Google\Chrome\Application\chrome.exe" --user-data-dir="E:\TEMP" --disable-web-security  --disable-background-timer-throttling --disable-extensions --disable-popup-blocking --bwsi --no-first-run --no-default-browser-check --disable-default-apps --disable-translate`
3. Run `flutter run -d web-server --web-port 4689 --web-hostname 127.0.0.1`
4. Open http://127.0.0.1:4689 from the Chrome

